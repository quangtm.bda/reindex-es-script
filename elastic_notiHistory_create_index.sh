#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index fplay-push-notification-by-history"
elastic_host=$1

curl -X PUT "$elastic_host/fplay-push-notification-by-history/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 1,
    "number_of_replicas" : 0,
    "refresh_interval": "10m"
  },
  "mappings": {
    "properties": {
      "item_id": {
        "type": "keyword"
      },
      "users": {
        "type": "keyword",
        "index": false
      }
    }
  }
}
'
