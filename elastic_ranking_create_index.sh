#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index ranking"
elastic_host=$1

curl -X PUT "$elastic_host/ranking/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 1,
    "number_of_replicas" : 0,
    "max_shingle_diff" : 4,
    "similarity": {
      "default": {
        "type": "BM25"
      }
    }
  },
  "mappings": {
    "properties": {
      "search_id": {
        "type": "long"
      },
      "source_key": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
'
