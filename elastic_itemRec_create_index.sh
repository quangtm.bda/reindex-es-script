#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index fplay-item-rec"
elastic_host=$1

curl -X PUT "$elastic_host/fplay-item-rec/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 1,
    "number_of_replicas" : 0
  },
  "mappings": {
    "properties": {
      "item": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "rec": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "type": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
'
