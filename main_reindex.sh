#!/bin/bash
elastic_source_host="http://sourcehost:9200"
elastic_dest_host="http://desthost:9200"

sync_actor() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'actor'
}

sync_cb_personalized() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'fplay-cb-personalized'
}

sync_channel() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'channels_version_1'
}

sync_item_rec() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'fplay-item-rec'
}

sync_noti_history() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'fplay-push-notification-by-history'
}

sync_official() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'official_version_1'
}

sync_personalized_structures() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'fplay-personalized-structures'
}

sync_ranking() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'ranking'
}

sync_search_personalized() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'fplay-search-personalized-keyword'
}

sync_keyword_dev_version_4() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'keyword_dev_version_4'
}

sync_tvprogram() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'tv_program'
}

sync_vod_rec_platform() {
  sh elastic_reindex.sh -d $elastic_dest_host -s $elastic_source_host -i 'fplay-user-vod-rec-platform' -bs 10
}

case $1 in
  all)
    sync_actor
    sync_cb_personalized
    sync_channel
    sync_item_rec
    sync_noti_history
    sync_official
    sync_personalized_structures
    sync_ranking
    sync_search_personalized
    sync_tvprogram
    sync_vod_rec_platform
    ;;
  actor)
    sync_actor
    ;;
  fplay-cb-personalized)
    sync_cb_personalized
    ;;
  channels_version_1)
    sync_channel
    ;;
  fplay-item-rec)
    sync_item_rec
    ;;
  fplay-push-notification-by-history)
    sync_noti_history
    ;;
  official_version_1)
    sync_official
    ;;
  fplay-personalized-structures)
    sync_personalized_structures
    ;;
  ranking)
    sync_ranking
    ;;
  fplay-search-personalized-keyword)
    sync_search_personalized
    ;;
  keyword_dev_version_4)
    sync_keyword_dev_version_4
    ;;
  tv_program)
    sync_tvprogram
    ;;
  fplay-user-vod-rec-platform)
    sync_vod_rec_platform
    ;;
  *)
    echo -n "run with [all | actor | fplay-cb-personalized | channels_version_1 | fplay-item-rec | fplay-push-notification-by-history | official_version_1 | fplay-personalized-structures | ranking | fplay-search-personalized-keyword | tv_program | fplay-user-vod-rec-platform | keyword_dev_version_4]"
    ;;
esac
