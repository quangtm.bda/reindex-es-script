#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index official_version_1"
elastic_host=$1

#curl -X POST "$elastic_host/official_version_1/_close?pretty"
curl -X PUT "$elastic_host/official_version_1/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 1,
    "number_of_replicas" : 0,
    "max_shingle_diff" : 4,
    "similarity": {
      "default": {
        "type": "BM25"
      }
    },
    "analysis": {
      "filter": {
        "my_ascii_folding": {
          "type": "asciifolding",
          "preserve_original": "true"
        },
        "synonym_filter": {
          "type": "synonym",
          "synonyms": [
            "lãng mạn, tình cảm, gia đình",
          "hành động,phiêu_lưu",
          "khoa học viễn tưởng, du hành thời gian, giả tưởng, siêu nhiên",
          "rùngrợn, hồi hộp, kinh dị, bí ẩn",
          "lịch sử,tài liệu",
          "hài hước, vui vẻ",
          "7, bảy"
          ]
        },
        "Edge_NGRAM": {
          "token_chars": [
            "letter",
          "digit",
          "whitespace",
          "punctuation",
          "symbol"
          ],
          "min_gram": "2",
          "type": "edge_ngram",
          "max_gram": "10"
        },
        "text_filter": {
          "split_on_numerics": "true",
          "generate_word_parts": "true",
          "preserve_original": "true",
          "generate_number_parts": "true",
          "catenate_all": "true",
          "split_on_case_change": "true",
          "type": "word_delimiter",
          "type_table": [
            "& => DIGIT"
          ],
          "catenate_numbers": "true"
        },
        "filter_shingle": {
          "max_shingle_size": "5",
          "min_shingle_size": "2",
          "output_unigrams": "true",
          "type": "shingle"
        }
      },
      "analyzer": {
        "edge_ngram_analyzer": {
          "filter": [
            "lowercase",
          "text_filter",
          "Edge_NGRAM",
          "my_ascii_folding"
          ],
          "type": "custom",
          "tokenizer": "standard"
        },
        "synonym_analyzer": {
          "filter": [
            "lowercase",
          "synonym_filter"
          ],
          "type": "custom",
          "tokenizer": "standard"
        },
        "viet_analyzer": {
          "filter": [
            "lowercase",
          "text_filter",
          "filter_shingle",
          "my_ascii_folding"
          ],
          "type": "custom",
          "tokenizer": "standard"
        },
        "vietname_analyzer_search": {
          "filter": [
            "lowercase"
          ],
          "type": "custom",
          "tokenizer": "standard"
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "GEOIP_COUNTRY_CODE_availability": {
        "type": "text",
        "similarity": "boolean",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "keyword"
      },
      "actors": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "actors_ed": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "edge_ngram_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "age_min": {
        "type": "long"
      },
      "all_view_count": {
        "type": "float"
      },
      "directors": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "duration": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "episode_current": {
        "type": "long"
      },
      "episode_latest": {
        "type": "float"
      },
      "episode_total": {
        "type": "long"
      },
      "episode_type": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "extracted_tags_genre": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "is_hot_clip_trailer": {
        "type": "float"
      },
      "isp": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "nation": {
        "type": "text",
        "similarity": "boolean",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "keyword"
      },
      "platforms": {
        "type": "text",
        "similarity": "boolean",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "keyword"
      },
      "release": {
        "type": "date",
        "format": "yyyy"
      },
      "ribbon_logo": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "ribbon_partner": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "ribbon_payment": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "search_view_count": {
        "type": "float"
      },
      "searchable": {
        "type": "long"
      },
      "small_img": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "source_provider": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "standing_img": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "status": {
        "type": "long"
      },
      "structure_id": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "subtracted_days": {
        "type": "float"
      },
      "sync_platforms": {
        "type": "long"
      },
      "title": {
        "index_phrases": true,
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "title_origin": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "title_origin_ed": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "edge_ngram_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "title_vie": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "type": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "verimatrix": {
        "type": "long"
      }
    }
  }
}
'
#curl -X POST "$elastic_host/official_version_1/_open?pretty"
