#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index fplay-user-vod-rec-platform"
elastic_host=$1

curl -X PUT "$elastic_host/fplay-user-vod-rec-platform/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 18,
    "number_of_replicas" : 0,
    "refresh_interval": "10m"
  },
  "mappings": {
    "properties": {
      "cate": {
        "type": "keyword"
      },
      "id": {
        "type": "keyword"
      },
      "recoms": {
        "properties": {
          "platforms": {
            "type": "keyword",
            "index": false
          },
          "rec": {
            "type": "keyword",
            "index": false
          }
        }
      }
    }
  }
}
'
