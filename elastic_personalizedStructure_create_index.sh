#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index fplay-personalized-structures"
elastic_host=$1

curl -X PUT "$elastic_host/fplay-personalized-structures/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 1,
    "number_of_replicas" : 0,
    "refresh_interval": "10m"
  },
  "mappings": {
    "properties": {
      "structures": {
        "type": "text",
        "index": false,
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "user_id": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      }
    }
  }
}
'
