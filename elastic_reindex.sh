#!/bin/bash

reindex_batch_size=1000

if [ "$#" -eq  "0" ];
then
  echo "No arguments supplied"
  exit
fi

while [ "$#" -gt 0 ]; do
    case $1 in
        -s|--source_host) elastic_source_host="$2"; shift ;;
        -d|--dest_host) elastic_dest_host="$2"; shift ;;
        -i|--index) elastic_index="$2"; shift ;;
        -bs|--batch_size) reindex_batch_size="$2"; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

if [ -z $elastic_source_host ] || [ -z $elastic_dest_host ] || [ -z $elastic_index ];
then
  echo "Script is not running. source_host, dest_host and index are required ."
  exit
fi

echo "running reindex $elastic_index"

curl -X POST "$elastic_dest_host/_reindex?pretty&wait_for_completion=false&timeout=2m" -H 'Content-Type: application/json' -d"
{
  \"source\": {
    \"remote\": {
      \"host\": \"$elastic_source_host\",
      \"socket_timeout\": \"5m\",
      \"connect_timeout\": \"10s\"
    },
    \"index\": \"$elastic_index\",
    \"size\": $reindex_batch_size
  },
  \"dest\": {
    \"index\": \"$elastic_index\"
  }
}
"

