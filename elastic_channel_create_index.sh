#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index channels_version_1"
elastic_host=$1

curl -X PUT "$elastic_host/channels_version_1/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 1,
    "number_of_replicas" : 0,
    "max_shingle_diff" : 4,
    "similarity": {
      "default": {
        "type": "BM25"
      }
    },
    "analysis": {
      "filter": {
        "my_ascii_folding": {
          "type": "asciifolding",
          "preserve_original": "true"
        },
        "Edge_NGRAM": {
          "token_chars": [
            "letter",
            "digit",
            "whitespace",
            "punctuation",
            "symbol"
          ],
          "min_gram": "2",
          "type": "edge_ngram",
          "max_gram": "10"
        },
        "text_filter": {
          "split_on_numerics": "true",
          "generate_word_parts": "true",
          "preserve_original": "true",
          "generate_number_parts": "true",
          "catenate_all": "true",
          "split_on_case_change": "true",
          "type": "word_delimiter",
          "type_table": [
            "& => DIGIT"
          ],
          "catenate_numbers": "true"
        },
        "filter_shingle": {
          "max_shingle_size": "5",
          "min_shingle_size": "2",
          "output_unigrams": "true",
          "type": "shingle"
        }
      },
      "analyzer": {
        "edge_ngram_analyzer": {
          "filter": [
            "lowercase",
            "text_filter",
            "Edge_NGRAM",
            "my_ascii_folding"
          ],
          "type": "custom",
          "tokenizer": "standard"
        },
        "viet_analyzer": {
          "filter": [
            "lowercase",
            "text_filter",
            "filter_shingle",
            "my_ascii_folding"
          ],
          "type": "custom",
          "tokenizer": "standard"
        },
        "vietname_analyzer_search": {
          "filter": [
            "lowercase"
          ],
          "type": "custom",
          "tokenizer": "standard"
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "GEOIP_COUNTRY_CODE_availability": {
        "type": "text",
        "similarity": "boolean",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "keyword"
      },
      "abbreviation": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "alias_name": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "description": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "group_id": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "imgPath": {
        "type": "text",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        }
      },
      "name": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "name_ed": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "edge_ngram_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "normalize": {
        "type": "text",
        "similarity": "BM25",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "viet_analyzer",
        "search_analyzer": "vietname_analyzer_search"
      },
      "platforms": {
        "type": "text",
        "similarity": "boolean",
        "fields": {
          "keyword": {
            "type": "keyword",
            "ignore_above": 256
          }
        },
        "analyzer": "keyword"
      },
      "status": {
        "type": "long"
      }
    }
  }
}
'
#curl -X POST "$elastic_host/official_version_1/_open?pretty"
