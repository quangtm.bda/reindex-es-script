#!/bin/bash

if [ -z $1 ]; then
  echo "Script is not running. elastic_host required ."
  exit
fi

echo "running create index cb-personalized"
elastic_host=$1

curl -X PUT "$elastic_host/fplay-cb-personalized/?pretty" -H 'Content-Type: application/json' -d'
{
  "settings": {
    "number_of_shards" : 4,
    "number_of_replicas" : 0
  },
  "mappings": {
    "properties": {
      "recoms": {
        "type": "keyword",
        "index": false
      },
      "user_id": {
        "type": "keyword"
      }
    }
  }
}
'
