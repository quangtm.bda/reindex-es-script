#!/bin/bash
elastic_source_host="http://sourcehost:9200"
elastic_dest_host="http://desthost:9200"

sync_actor() {
  sh elastic_actor_create_index.sh $elastic_dest_host 
}

sync_cb_personalized() {
  sh elastic_cbPersonalized_create_index.sh $elastic_dest_host 
}

sync_channel() {
  sh elastic_channel_create_index.sh $elastic_dest_host 
}

sync_item_rec() {
  sh elastic_itemRec_create_index.sh $elastic_dest_host 
}

sync_noti_history() {
  sh elastic_notiHistory_create_index.sh $elastic_dest_host 
}

sync_official() {
  sh elastic_official_create_index.sh $elastic_dest_host 
}

sync_personalized_structures() {
  sh elastic_personalizedStructure_create_index.sh $elastic_dest_host 
}

sync_ranking() {
  sh elastic_ranking_create_index.sh $elastic_dest_host 
}

sync_search_personalized() {
  sh elastic_searchPersonalized_create_index.sh $elastic_dest_host 
}

sync_keyword_dev_version_4() {
  sh elastic_keywordDevVersion4_create_index.sh $elastic_dest_host 
}

sync_tvprogram() {
  sh elastic_tvprogram_create_index.sh $elastic_dest_host 
}

sync_vod_rec_platform() {
  sh elastic_vodRecPlatform_create_index.sh $elastic_dest_host 
}

case $1 in
  all)
    sync_actor
    sync_cb_personalized
    sync_channel
    sync_item_rec
    sync_noti_history
    sync_official
    sync_personalized_structures
    sync_ranking
    sync_search_personalized
    sync_keyword_dev_version_4
    sync_tvprogram
    sync_vod_rec_platform
    ;;
  actor)
    sync_actor
    ;;
  fplay-cb-personalized)
    sync_cb_personalized
    ;;
  channels_version_1)
    sync_channel
    ;;
  fplay-item-rec)
    sync_item_rec
    ;;
  fplay-push-notification-by-history)
    sync_noti_history
    ;;
  official_version_1)
    sync_official
    ;;
  fplay-personalized-structures)
    sync_personalized_structures
    ;;
  ranking)
    sync_ranking
    ;;
  fplay-search-personalized-keyword)
    sync_search_personalized
    ;;
  keyword_dev_version_4)
    sync_keyword_dev_version_4
    ;;
  tv_program)
    sync_tvprogram
    ;;
  fplay-user-vod-rec-platform)
    sync_vod_rec_platform
    ;;
  *)
    echo -n "run with [all | actor | fplay-cb-personalized | channels_version_1 | fplay-item-rec | fplay-push-notification-by-history | official_version_1 | fplay-personalized-structures | ranking | fplay-search-personalized-keyword | tv_program | fplay-user-vod-rec-platform | keyword_dev_version_4]"
    ;;
esac
